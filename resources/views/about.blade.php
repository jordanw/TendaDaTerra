@extends('layout.layout')

@section('content')

    <div class="col-sm-12" style="margin-top: 10px">
        <section id="about_us">
            <h1 class="green"><span class="brown">Tenda</span> <span class="green">da Terra</span></h1>
            <h2 class="green">Nossa história</h2>

            <p>
                O Núcleo de Orientação Ecológica, Pesquisa e Divulgação do Meio Ambiente - NOEPEMA foi criado e implantado por universitários e técnicos da área de meio ambiente, em 05 de junho de 1982, dia Mundial do Meio Ambiente, na sede da Fundação Projeto Rondon, Coordenação Estadual do Maranhão, localizada na Rua da Saavedra, 147, Centro, CEP: 65.010-630, no município de São Luís, estado do Maranhão.
                A Organização Não-Governamental, com CNPJ 63.442.271/0001-17, foi instalada e sediada na Avenida Getúlio Vargas, 1765, salas 7 e 8, Canto da Fabril, CEP: 65.020-330, com telefone (98)221-1046, Caixa Postal 1093, Agência Praça João Lisboa, Fax: (98)2328388, e-mail, tendadaterra@elo.com.br, possuindo um site de divulgação: www.tendadaterra.com.br, no sentido de proteger, defender, preservar e/ou conservar o patrimônio ambiental, histórico, artístico e cultural maranhense.
            </p>
        </section>
    </div>
    </div>

@endsection