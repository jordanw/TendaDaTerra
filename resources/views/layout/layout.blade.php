<!DOCTYPE HTML>
<html>
<head>
    <meta http-equiv='Content-Type' content='text/html'; charset='utf-8' />
    <meta http-equiv="X-UA-Compatible" content="IE-edge"/>
    <title>Tenda da Terra</title>
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.1.1/css/bootstrap.min.css" integrity="sha384-WskhaSGFgHYWDcbwN70/dfYBj47jz9qbsMId/iRN3ewGhXQFZCSftd1LZCfmhktB" crossorigin="anonymous">
    <script src="https://code.jquery.com/jquery-3.3.1.slim.min.js" integrity="sha384-q8i/X+965DzO0rT7abK41JStQIAqVgRVzpbzo5smXKp4YfRvH+8abtTE1Pi6jizo" crossorigin="anonymous"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.3/umd/popper.min.js" integrity="sha384-ZMP7rVo3mIykV+2+9J3UJ46jBk0WLaUAdn689aCwoqbBJiSnjAK/l8WvCWPIPm49" crossorigin="anonymous"></script>
    <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.1.1/js/bootstrap.min.js" integrity="sha384-smHYKdLADwkXOn1EmN1qk/HfnUcbVRZyYmZ4qpPea6sjB/pTJ0euyQp0Mk8ck+5T" crossorigin="anonymous"></script>
    <link rel="stylesheet" href="{{ asset('css/stylesheet.css') }}" type="text/css">
    <link rel="stylesheet" href="{{ asset('css/content.css') }}" type="text/css">


</head>
<body>

<header>
    <!--    THE TOP BAR OF LINKS OF SOCIAL NETWORKS -->
    <div class="row">
        <div class="col-sm-12">
            <nav id="web-social" class="navbar navbar-default">
                <!--<div class="navbar-collapse" id="lista">-->
                <ul class="nav navbar-nav social" id="social">
                    <li>
                        <a href="https://www.facebook.com/ongtendadaterra/" target="_blank">
                            <img src="{{ URL::to('/assets/img/socialNetworks/facebook.png') }}" alt="Facebook">
                        </a>
                    </li>
                    <li>
                        <a href="https://www.instagram.com/?hl=pt-br" target="_blank">
                            <img src="{{ URL::to('/assets/img/socialNetworks/instagram.png') }}" alt="instagram">
                        </a>
                    </li>
                    <li>
                        <a href="https://www.youtube.com" target="_blank">
                            <img src="{{ URL::to('/assets/img/socialNetworks/youtube-logo.png') }}" alt="Youtube">
                        </a>
                    </li>
                </ul>

                <div class= "navbar-collapse" id="busca">
                    <form action="/action_page.php" id="search">
                        <div class="input-group">
                            <input type="text" class="form-control" placeholder="Pesquisar" name="search">
                            <div class="input-group-btn">
                                <button class="btn btn-success" type="submit">Buscar</button>
                            </div>
                        </div>
                    </form>
                </div><!-- fim navbar-collapse busca-->
            </nav>
        </div>
    </div>


        <nav class="navbar navbar-expand-lg navbar-light bg-light" id="nav-large">
                <h1><a href="{{route('index')}}">
                        <img src="{{ URL::to('/assets/img/painel/logo.jpg') }}" alt="Tenda da Terra" id="logo-index" class="rounded-circle">
                    </a></h1>
                <ul class="nav navbar-nav float-right" id="ul-index">
                    <li><a href="{{route('index')}}">Home</a></li>
                    <li><a href="{{route('about')}}">Sobre</a></li>
                    <li class="nav-item dropdown">
                        <a class="nav-link dropdown-toggle" href="#" id="navbarDropdown" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                            Projetos
                        </a>
                        <div class="dropdown-menu" aria-labelledby="navbarDropdown">
                            <a class="dropdown-item" href="#" style="color: #000">Educação Ambiental Infantil</a>
                            <a class="dropdown-item" href="#"style="color: #000">Educação Ambiental EAD</a>
                            <div class="dropdown-divider"></div>
                        </div>
                    </li>
                    <li><a href="http://blogcontraponto.com.br" target="_blank">Blog</a></li>
                    <li><a href="{{route('contact')}}">Contato</a></li>
                </ul>
            </nav>

        </div><!-- fim col-sm-12-->
    </div><!-- fim de row2-->
    <!--</div>-->
</header>
<div style="position: relative">
    <aside id="side-bar">
        <div>
            <figure>
                <img src="https://i0.wp.com/www.revistabula.com/wp/wp-content/uploads/2018/06/Pele.jpg?fit=300%2C172&ssl=1"/>
                <figcaption>os 10 momentos maisicônicos da história das copas</figcaption>
            </figure>
        </div>
        <div>
            <figure>
                <img src="https://i1.wp.com/www.revistabula.com/wp/wp-content/uploads/2018/06/Perfeita-pra-Voce.jpg?fit=300%2C172&ssl=1"/>
                <figcaption>Melhores filmes de 2018 para assistir</figcaption>
            </figure>
        </div>
        <div>
            <figure>
                <img src="https://i2.wp.com/www.revistabula.com/wp/wp-content/uploads/2018/06/St-Christopher’s-Paris-1.jpg?fit=300%2C172&ssl=1"/>
                <figcaption>20 hotéis incrivéis em 20 países que custam menos de 40 euros</figcaption>
            </figure>
        </div>
        <div>
            <figure>
                <img src="https://i0.wp.com/www.revistabula.com/wp/wp-content/uploads/2018/06/The-Invitation1-1.jpg?fit=300%2C172&ssl=1"/>
                <figcaption>15 filmes para ver na Netflix</figcaption>
            </figure>
        </div>

    </aside>
</div>
    <div class="container">
        @yield('content')
    </div>

<footer>

    <h2 id="sobre">SOBRE</h2>

    <p id="p-f">O Núcleo de Orientação Ecológica, Pesquisa e Divulgação do Meio Ambiente - NOEPEMA foi criado e implantado por universitários e técnicos da área de meio ambiente, em 05 de junho de 1982, dia Mundial do Meio Ambiente.</p>

    <h2 id="cont">CONTATO</h2>

    <ul id="contato">
        <li>(98) 000-000</li>
        <li><a href="contact.php">Fale conosco</a></li>
    </ul>

    <nav class="navbar" id="nav-footer" >
        <ul class="nav pull-right" id="list-footer">
            <li>
                <a href="facebook.com">
                    <img src="{{ URL::to('/assets/img/socialNetworks/facebook.png') }}" alt="Facebook">
                </a>
            </li>
            <li><a href="https://www.instagram.com/?hl=pt-br" target="_blank">
                    <img src="{{ URL::to('/assets/img/socialNetworks/instagram.png') }}" alt="Instagram">
                </a></li>
            <li><a href="https://www.youtube.com">
                    <img src="{{ URL::to('/assets/img/socialNetworks/youtube-logo.png') }}" alt="You Tube">
                </a>
            </li>
        </ul>
    </nav>

    <h2 id="end">ENDEREÇO</h2>
    <address>
        <strong>Endereço:</strong> Av. dos Portugueses, 1966 - Vila Bacanga, São Luís - MA, <br/>
        <strong><abbr title="Código de endereçamento postal" >CEP:</abbr></strong> 65065-545<br/>
        <strong>Telefone:</strong> (98) 3272-8000<br/>
        <strong>Fundada em:</strong> 21 de outubro de 1966</address><br/>
    </address>

</footer>
</body>
</html>