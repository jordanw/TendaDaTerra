@extends('layout.layout')

@section('content')
    <div id="content">
        <figure>
            <img src="https://i1.wp.com/www.revistabula.com/wp/wp-content/uploads/2018/07/1.jpg?resize=610%2C350&ssl=1"/>
            <figcaption>
                <h2>
                    <a href="#">A história do gênio da grande área que quase pôs fim ao reinado de Pelé</a>
                </h2>

                <p>“Rei, Rei, Rei, Reinaldo é nosso Rei.” Era assim que os torcedores do Atlético Mineiro saudavam seu principal craque.
                    O centroavante também brilhou na Seleção Brasileira. Era o Neymar de seu tempo, mas, caçado pelos zagueiros e beques,
                    teve sua carreira abreviada (parou aos 31 anos). O livro “Punho Cerrado — A História do Rei” (Letramento, 307 páginas),
                    de Philipe van R. Lima, filho do atacante, poderia ter se tornado uma hagiografia, contando as coisas boas e escondendo
                    as ruins, mas o autor relata tudo (ou quase) — inclusive o vício em cocaína.</p>
            </figcaption>
        </figure>

        <figure>
            <img src="https://i0.wp.com/www.revistabula.com/wp/wp-content/uploads/2017/05/Dorothea-Lange.jpg?resize=610%2C350&ssl=1"/>
            <figcaption>
                <h2>
                    <a href="#">Biblioteca disponibiliza 2,2 milhões de arquivos dos 100 primeiros anos da fotografia</a>
                </h2>

                <p>A biblioteca virtual Europeana lançou uma coleção capaz de enlouquecer qualquer amante de fotografia.
                    A “Europeana Photography” reúne 2,2 milhões de arquivos correspondentes aos primeiros 100 anos da
                    história da fotografia, que podem ser vistos online e estão disponíveis parra download gratuito.
                    A coleção é composta por imagens e documentos de 50 instituições europeias, localizadas em 34 diferentes
                    países.</p>
            </figcaption>
        </figure>

        <figure>
            <img src="https://i0.wp.com/www.revistabula.com/wp/wp-content/uploads/2018/06/Virado-a-Paulista.jpg?resize=610%2C350&ssl=1"/>
            <figcaption>
                <h2>
                    <a href="#">Viaje pelo Brasil com a comida típica de cada Estado</a>
                </h2>

                <p>A culinária é parte importante da cultura de um povo. Prova disso são os pratos típicos do Brasil,
                    que representam as diferentes nuances dos conhecimentos, crenças e costumes de seus habitantes.
                    Pensando nisso, a Bula selecionou comidas típicas de todas as regiões do país. A lista inclui um prato para
                    cada estado brasileiro e o Distrito Federal. Não foram consideradas as receitas que já estão amplamente
                    difundidas e reconhecidas, como o pão-de-queijo de Minas Gerais, o acarajé baiano, e a buchada de bode,
                    que é típica de todo o Nordeste.</p>
            </figcaption>
        </figure>

    </div>
@endsection