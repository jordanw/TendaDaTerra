@extends('layout.layout')

@section('content')

    <section id="contact" style="margin-top: 10px">
        <h1>Fale com a <span class="brown">Tenda</span> <span class="green">da Terra</span></h1>
        <form action="controle/email.php" method="POST" id="form_contact">
            <div class="form-group">
                <label for="name">Nome:</label><br>
                <input type="text" name="name" id="name" placeholder="Seu nome" class="form-control">
            </div>
            <div class="form-group">
                <label for="">Email:</label><br>
                <input type="email" name="email" id="email" placeholder="Seu email" aria-describedby="_email" class="form-control" title="exemplo@email.com">
            </div>

            <div class="form-group">
                <label for="">Assunto:</label><br>
                <input type="" name="subject" id="subject" placeholder="Assunto" class="form-control">
            </div>
            <div class="form-group">
                <label for="message">Mensagem:</label>
                <textarea rows="5" name="message" id="message" class="form-control">

					</textarea>
            </div>
            <div class="form-group">
                <input type="submit" value="Enviar" class="btn btn-success" class="form-control">
            </div>
        </form>
    </section>

@endsection
