<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/contato', function (){
    return view('contact');
})->name('contact');

Route::get('/sobre', function (){
   return view('about');
})->name('about');

Route::get('/home', function ()
{
    return view('index');
})->name('index');

Route::get('/', function () {
    return view('layout/layout');
});
